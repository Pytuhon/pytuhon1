import random
import json
from data import name


def zodiac_sign(day, month):
   if month == '12':
      astro_sign = 'Sagittarius' if (day < 22) else 'capricorn'
   elif month == '1':
      astro_sign = 'Capricorn' if (day < 20) else 'aquarius'
   elif month == '2':
      astro_sign = 'Aquarius' if (day < 19) else 'pisces'
   elif month == '3':
      astro_sign = 'Pisces' if (day < 21) else 'aries'
   elif month == '4':
      astro_sign = 'Aries' if (day < 20) else 'taurus'
   elif month == '5':
      astro_sign = 'Taurus' if (day < 21) else 'gemini'
   elif month == '6':
      astro_sign = 'Gemini' if (day < 21) else 'cancer'
   elif month == '7':
      astro_sign = 'Cancer' if (day < 23) else 'leo'
   elif month == '8':
      astro_sign = 'Leo' if (day < 23) else 'virgo'
   elif month == '9':
      astro_sign = 'Virgo' if (day < 23) else 'libra'
   elif month == '10':
      astro_sign = 'Libra' if (day < 23) else 'scorpio'
   elif month == '11':
      astro_sign = 'scorpio' if (day < 22) else 'sagittarius'
   return astro_sign


i = 1

database = {}
while i <= 10:

   year = random.randint(1900, 2021)
   if year % 400 == 0:
      leapYear = True
   elif year % 100 == 0:
      leapYear = False
   elif year % 4 == 0:
      leapYear = True
   else:
      leapYear = False

   pet = random.choice(('dog', 'cat', 'mouse'))

   eyes = random.choice(('green', 'blue', 'red'))

   genName = random.choice(name)

   # Что ты тут делаешь не так, как ты думаешь?
   month = random.choice(('1', '2', '3', '4', '5', '6',
                        '7', '8', '9', '10', '11', '12'))
   if month == 2:
      if leapYear == True:
         day = random.randint(1, 29)
   elif month == 2:
      day = random.randint(1, 28)
   # Не ошибка, но замечание: 
   # в каждом месяца отличном от 2 - у тебя 31 день?:)
   else:
      day = random.randint(1, 31)

   # Можно воспользоваться обратной логикой, к примеру: 
   # ты можешь генерировать цифру возраста и её вычитать 
   # из года, тем самым ограничив возрастной потолок.
   age = 2021 - year

   database[genName] = {
      'возраст': age,
      'телефон': '',
      'цвет_волос': '',
      'цвет_глаз': eyes,
      'домашнее_животное': pet,
      'знак_зодиака': zodiac_sign(day, month),
      'день_рождения': f"{day:0>2}.{month:0>2}.{year}"
   }
   i += 1


print(
   json.dumps(
      database, indent=2,
      ensure_ascii=False, skipkeys=True,
      allow_nan=True
   )
)
